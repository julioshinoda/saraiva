package com.saraiva.api.repository;

import com.saraiva.api.model.Livro;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LivroRepository extends JpaRepository<Livro,Long> {

    Optional<Livro> findBySku(Long sku);


    Optional<List<Livro>> findAllByPriceIsLessThanEqual(Double price);

    Optional<List<Livro>> findAllByPriceIsLessThanEqual(Double price, Pageable pageable);

}

