package com.saraiva.api.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saraiva.api.exception.ResourceBadRequestException;
import com.saraiva.api.exception.ResourceNotFoundException;
import com.saraiva.api.model.Livro;
import com.saraiva.api.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.Null;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class LivroController {

    @Autowired
    LivroRepository livroRepository;

    @GetMapping("/book")
    public List<Livro> getAllLivro(@RequestParam(required = false, value = "price") Double price,
                                   @RequestParam(required = false, value = "limit",defaultValue = "0") Integer limit) {

        if (limit != 0) {
            Pageable pageable = new PageRequest(0, limit);

            if (price != null) {
                return livroRepository.findAllByPriceIsLessThanEqual(price,pageable).orElseThrow(() -> new ResourceNotFoundException("Livro", "price", price));

            }


            return livroRepository.findAll(pageable).getContent();
        }

        if (price != null) {
            return livroRepository.findAllByPriceIsLessThanEqual(price).orElseThrow(() -> new ResourceNotFoundException("Livro", "price", price));

        }


        return livroRepository.findAll();

    }

    @GetMapping("/book/{sku}")
    public Livro getLivroBySku(@PathVariable(value = "sku") Long sku) {

        return livroRepository.findBySku(sku).orElseThrow(() -> new ResourceBadRequestException("Livro", "sku", sku));

    }

    @PostMapping("/book")
    public ResponseEntity createLivro(Long sku) {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response
                = restTemplate.getForEntity("https://api.saraiva.com.br/sc/produto/pdp/" + sku + "/0/0/1/", String.class);


        try {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());

            Livro livro = new Livro();
            livro.setBrand(root.path("brand").asText());
            livro.setName(root.path("name").asText());
            livro.setSku(root.path("sku").asLong());
            livro.setPrice(Double.parseDouble(root.path("price")
                    .path("bestPrice")
                    .path("value")
                    .asText()
                    .replace(",", ".")));

            livroRepository.save(livro);
            return new ResponseEntity(HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        }

    }

    @DeleteMapping("/book/{sku}")
    public ResponseEntity deleteLivro(@PathVariable(value = "sku") Long sku) {


        try {
            Optional<Livro> livro = livroRepository.findBySku(sku);
            livroRepository.delete(livro.get());
            return new ResponseEntity(HttpStatus.NO_CONTENT);

        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }


}
