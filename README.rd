Projeto API de consumo de livro.

O projeto foi montado conforme as instruções fornecidas no documento Desafio Técnico Saraiva - Analista JAVA.pdf

Banco de dados
Para o banco de dados eu utilizei o docker-compose. Então para rodar o banco de dados precisa ter o docker e docker compose instalados na maquina.
Após a instalação, dentro do terminal entra na raiz desse projeto (onde estará o arquivo docker-compose.yml) e utilize o comando "docker-compose up"para subir o banco de dados.


Para rodar a aplicação também pelo terminal digite "mvn spring-boot:run"


